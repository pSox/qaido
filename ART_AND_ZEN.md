# The Art add the Zen of coding in `qaido`

This document tries to describe the philosophy of coding in the `quaido` project.

## Fundamentals

1. Simple should be simple
    What we mean is that if you performing a simple task it should be simple to do.
2. Resilient must be easy
    While it is not always desirable to auto-recover from an error, it must be possible and easy to do so.
3. Complex must be possible
   Just because it is simple to perform simple tasks, does not mean it is not possible to perform complex tasks.

## Plugins and APIs

### Plugins

### APIs

## Languages and Coding Styles

### Languages

The project language is [go][go], however it is a plugin based architecture and as such we would prefer that each plugin be written in the language being supported; so java and maven support should be written in java, c in c using Makefiles, C++ using c++ and CMake and Jenkins in ruby and so on.

If you do not want to use the language you are supporting, then please use go.

### Coding Styles

Coding style should follow the standards of the language being used.

For [go][go] we use:

* [golint][golint]
* [go vet][go_vet]
* [gofmt][gofmt]

For Configuration we prefer yaml.

[go]: https://golang.org/
[golint]: https://github.com/golang/lint
[go_vet]: https://golang.org/cmd/vet/
[gofmt]: https://golang.org/cmd/gofmt/