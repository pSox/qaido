# The `qaido` Code of Conduct

A version of this document [can be found online](https://bitbucket.org/pSox/qaido/src/master/CODE_OF_CONDUCT.md).

As the community grows this docuemnet will be alterd to fit the culture

## Conduct

1. We are committed to providing a friendly, safe and welcoming environment for all, regardless of level of experience, gender identity and expression, sexual orientation, disability, personal appearance, body size, race, ethnicity, age, religion, nationality, or other similar characteristic.
2. On IRC, please avoid using overtly sexual nicknames or other nicknames that might detract from a friendly, safe and welcoming environment for all.
3. Please be kind and courteous. There's no need to be mean or rude.
4. Respect that people have differences of opinion and that every design or implementation choice carries a trade-off and numerous costs. There is seldom a right answer.
5. Please keep unstructured critique to a minimum. If you have solid ideas you want to experiment with, make a fork and see how it works.
6. We will exclude you from interaction if you insult, demean or harass anyone. That is not welcome behaviour. We interpret the term "harassment" as including the definition in the [Citizen Code of Conduct](http://citizencodeofconduct.org) if you have any lack of clarity about what might be included in that concept, please read their definition. In particular, we don't tolerate behavior that excludes people in socially marginalized groups.
7. Likewise any spamming, trolling, flaming, baiting or other attention-stealing behaviour is not welcome.

In the `qaido` community we strive to go the extra step to look out for each other. Don't just aim to be technically unimpeachable, try to be your best self. In particular, avoid flirting with offensive or sensitive issues, particularly if they're off-topic; this all too often leads to unnecessary fights, hurt feelings, and damaged trust; worse, it can drive people away from the community entirely.

And if someone takes issue with something you said or did, resist the urge to be defensive. Just stop doing what it was they complained about and apologize. Even if you feel you were misinterpreted or unfairly accused, chances are good there was something you could've communicated better — remember that it's your responsibility to make your fellow contributers feel comfortable. Everyone wants to get along and we are all here first and foremost because we want to talk about cool technology. You will find that people will be eager to assume good intent and forgive as long as you earn their trust.

*Adapted from the [Rust Code of Conduct](https://www.rust-lang.org/conduct.html)
